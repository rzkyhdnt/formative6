import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Bogor implements Runnable{
    @Override
    public void run() {
        ArrayList<String> platNumber = new ArrayList<String>();
        String[] char1 = {"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
                        "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
                        "W", "X", "Y", "Z"};
        String[] char2 = char1;
        String identityFront = "F";
        ArrayList<String> identityBack = new ArrayList<>();

        for(int i = 0; i < char1.length; i++){
            for(int j=0; j < char2.length; j++){
                identityBack.add(char1[i] + char2[j]);
            }
        }

        for(int k=0; k < identityBack.size(); k++){
            for(int l = 0; l < 9_999; l++){
                platNumber.add(identityFront + " " + l + " " + identityBack.get(k));
            }
        }

        BufferedWriter createFile = null;
        try {
            createFile = new BufferedWriter(new FileWriter("bogor.txt"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            for(String str : platNumber){
                createFile.write(str + System.lineSeparator());
            }
            createFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
