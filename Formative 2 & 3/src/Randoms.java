import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Randoms {
    String name;
    String output;
    boolean same = false;


    public String setRandom(String name){
        this.name = name;
        ArrayList<Character> chars = new ArrayList<Character>();
        StringBuilder charToString = new StringBuilder();
        char[] charConvert = name.toCharArray();

        for(int i=0; i<charConvert.length; i++){
            chars.add(charConvert[i]);
        }

        Collections.shuffle(chars);

        for(Character c : chars){
            charToString.append(c);
        }
        output = charToString.toString();

        return output;
    }

    public void getRandom(){
        System.out.println("Setelah di acak             : " + output);
    }

    public void compareString(){
        char[] charConvert = name.toCharArray();
        char[] charReference = output.toCharArray();
        Arrays.sort(charConvert);
        Arrays.sort(charReference);

        for(int i =0; i<charConvert.length; i++){
            if(charConvert[i] == charReference[i]){
                same = true;
            } else {
                same = false;
                break;
            }
        }

        String names = same ? "Validasi nama               : " + name + "\n" : "Name not found!\n";

        System.out.println(names);
    }
}
